from django.shortcuts import render
from django.views.generic import ListView, DetailView
from encuesta.models import Survey, Question, Answer


class SurveyListView(ListView):
    """
    Vista Generica usada para listar las encuestas.
    :cvar template_name: nombre de la plantilla que renderizara.
    :cvar queryset: Lista de las encuestas que seran mostradas
    :cvar context_object_name: Encuestas en el contexto de la pagina
    """
    template_name = "HomeList.html"
    queryset = Survey.objects.all()
    context_object_name = "surveys"


class AnswersDetailView(DetailView):
    """
    Vista Generica usada para listar las respuestas de una encuesta recibe un pk
    :cvar model: Variable que guarda el modelo del cual se queire el detalle.
    :cvar template_name: nombre de la plantilla que renderizara.
    """
    model = Survey
    template_name = "AnswersByUser.html"

    def get_context_data(self, **kwargs):
        """
        Metodo que actualiza el contexto de la pagina
        :return: Nuevo contexto
        """
        context = super(AnswersDetailView, self).get_context_data(**kwargs)
        context["answers"] = Answer.objects.filter(
            question__survey=self.get_object()).order_by("user")
        return context


class SurveyDetailView(DetailView):
    """
    Vista Generica usada para detallar las preguntas y alternativas de una
        encuesta recibe un pk
    :cvar model: Variable que guarda el modelo del cual se queire el detalle.
    :cvar context_object_name: nombre del modelo que sera usado como contexto.
    :cvar template_name: nombre de la plantilla que renderizara.
    """
    model = Survey
    context_object_name = "survey"
    template_name = "SurveyDetail.html"

    def get_context_data(self, **kwargs):
        """
        Metodo que actualiza el contexto de la pagina
        :return: Nuevo contexto
        """
        context = super(SurveyDetailView, self).get_context_data(**kwargs)
        context["questions"] = Question.objects.filter(survey=self.get_object())
        return context


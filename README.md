# Sistema de Encuestas Dinámicas
El siguiente Proyecto es un Sistema Backend dinámico encargado de crear encuestas y registrar las respuestas de estas.
###Tecnologías
* Django Framework

## Instalacion

* Crea un Entorno Virtual para la intalacion de los paquetes necesarios con el siguiente comando
```bash
    python -m venv virtual-env
```

* Clona el proyecto desde el siguiente [Enlace](https://gitlab.com/pportugal190895/encuetaspaulportugalherencia.git)
```bash
    https://gitlab.com/pportugal190895/encuetaspaulportugalherencia.git
```

* Si tu sistema operativo es Windows ubicate en el fichero **virtual-env\Scripts** y ejecuta el siguiente comando para activar el entorno virtual, en Caso de ser Linux la ruta es **virtual-env\bin\activate**
```bash
    activate.bat  (windows)
    source activate.bat  (Linux)
``` 

* Una vez activado el Entorno Virtual es necesario instalar los paquetes, para esto regresa a la ubicacion del proyecto 
y ejecuta el siguiente comando
```bash
    pip install -r requirements.txt
```


* La estructura de archivos deberia tener la siguiente forma 
```
    ├── virtual-env
    │   ├── Include
    │   ├── Lib
    │   ├── Scripts(Windows)/Source(Linux)
    │   ├── Encuestas Proyect
    │   │   ├── ...
    │   │   ├── encuestas
    │   │   ├── manage.py
    │   │   ├── requirements.txt
    │   │   └── ...
    │   └── ...
    └──
```


* Ahora es necesario Migrar los modelos de la siguiente manera
```bash
    python manage.py migrate
```

* Creamos un superusuario con el siguiente comando; el super usuario por defecto es x@2.com y la contraseña es "a123456789b" 
```
    python manage.py createsuperuser
```

* Para finalizar iniciaremos el servidor y ejecutaremos el proyecto
```bash
    python manage.py runserver
```


## Sistema de Encuestas (Backend)
### Crear Encuestas 
* Dirigirse a la siguiente direccion en el navegador localhost:8000/admin 
* Loguearse con el Super Usuario.
* Crear una encuesta en el administrador de Django

### Visualizar las encuestas 
* Dirigirse a la siguiente direccion en el navegador localhost:8000

### Visualizar el detalle de la encuesta con sus alternativas
* Seleccionar la encuesta para ver los detalles o
* Dirigirse a la siguiente direccion en el navegador localhost:8000/detail/survey/1/

### Visualizar las respuestas de la encuesta 
* presionar el boton que dice Ver respuestas ó 
* Dirigirse a la siguiente direccion en el navegador localhost:8000/answers/survey/1/
